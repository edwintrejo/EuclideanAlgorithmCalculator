# EuclideanAlgorithmCalculator
It works to find the GCD from two numbers. It uses the Eucledian Algorithm.

[example] 88 and 54

88 = 54(1) + 34

54 = 34(1) + 20

34 = 20(1) + 14

20 = 14(1) + 6

14 = 6(2) + 2

6 = 2(3) + 0

gcd = 2

[example] 888 and 54

888 = 54(16) + 24

54 = 24(2) + 6

24 = 6(4) + 0

gcd = 6
